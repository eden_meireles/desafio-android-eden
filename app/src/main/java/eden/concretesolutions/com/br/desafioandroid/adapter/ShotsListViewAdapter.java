package eden.concretesolutions.com.br.desafioandroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.helper.RoundTransform;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;

public class ShotsListViewAdapter extends BaseAdapter {

    private final Context context;
    private final List<Shot> shots;

    public ShotsListViewAdapter(Context context, List<Shot> shots) {

        this.context = context;
        this.shots = shots;
    }

    @Override
    public int getCount() {
        return this.shots.size();
    }

    @Override
    public Object getItem(int position) {
        return this.shots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shot_single, null);
        }

        Shot tempShot = this.shots.get(position);

        ImageView mImageView = (ImageView) convertView.findViewById(R.id.shot);
        Picasso.with(context)
                .load(tempShot.getImage400Url())
                .into(mImageView);

        TextView mTitle = (TextView) convertView.findViewById(R.id.title);
        mTitle.setText(tempShot.getTitle());

        TextView mViewQtd = (TextView) convertView.findViewById(R.id.view_qtd);
        mViewQtd.setText( String.valueOf(tempShot.getViewsCount()) );

        return convertView;

    }
}