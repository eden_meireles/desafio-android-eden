package eden.concretesolutions.com.br.desafioandroid.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.helper.RoundTransform;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;

public class DescriptionShotDetailFragment extends Fragment {

    public DescriptionShotDetailFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.shot_description, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Shot shot = (Shot) bundle.getParcelable("shot");

            ImageView mImageView = (ImageView) view.findViewById(R.id.player_avatar);
            Picasso.with(getActivity())
                    .load(shot.getPlayer().getAvatarUrl())
                    .transform(new RoundTransform(100, 0))
                    .into(mImageView);

            TextView mTitle = (TextView) view.findViewById(R.id.player_name);
            mTitle.setText(shot.getPlayer().getName());

            TextView mDescription = (TextView) view.findViewById(R.id.shot_description);
            if(mDescription != null && shot.getDescription() != null)
                mDescription.setText(
                        Html.fromHtml(shot.getDescription())
                );
        }

    }
}
