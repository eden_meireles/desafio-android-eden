package eden.concretesolutions.com.br.desafioandroid.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.ShotZoomActivity;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;
import uk.co.senab.photoview.PhotoViewAttacher;

public class HeaderShotDetailFragment extends Fragment {

    PhotoViewAttacher mAttacher;
    Bundle bundle;
    Shot shot;

    public HeaderShotDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.shot_single, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.bundle = this.getArguments();
        if (this.bundle != null) {

            this.shot = (Shot) bundle.getParcelable("shot");

            final ImageView mImageView = (ImageView) view.findViewById(R.id.shot);
            Target target = new Target() {

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }

                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                    mImageView.setImageBitmap(bitmap);

                    Drawable image = mImageView.getDrawable();
                    mImageView.setImageDrawable(image);
                    mAttacher = new PhotoViewAttacher(mImageView);

                    mAttacher.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
                        @Override
                        public void onViewTap(View view, float v, float v1) {

                            // shot zoom in new activity
                            Intent intent = new Intent(getActivity(), ShotZoomActivity.class);
                            intent.putExtra("shot", shot);
                            startActivity(intent);
                        }
                    });
                }

            };

            Picasso.with(getActivity())
                    .load( shot.getImage400Url() )
                    .into(target);

            TextView mTitle = (TextView) view.findViewById(R.id.title);
            mTitle.setText(shot.getTitle());

            TextView mViewQtd = (TextView) view.findViewById(R.id.view_qtd);
            mViewQtd.setText( String.valueOf(shot.getViewsCount()) );

        }

    }


}
