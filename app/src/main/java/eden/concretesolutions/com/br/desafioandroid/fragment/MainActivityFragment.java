package eden.concretesolutions.com.br.desafioandroid.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.ShotDetailActivity;
import eden.concretesolutions.com.br.desafioandroid.adapter.ShotsListViewAdapter;
import eden.concretesolutions.com.br.desafioandroid.webservice.DribbbleApi;
import eden.concretesolutions.com.br.desafioandroid.webservice.RestClient;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Popular;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    final ArrayList<Shot> shots = new ArrayList<Shot>();
    private Integer page = 1;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ShotsListViewAdapter mAdapter = new ShotsListViewAdapter(getActivity(), shots);

        ListView shotsListView = (ListView) view.findViewById(R.id.shot_list);
        this.loadDribbleData(mAdapter, shotsListView, this.page);


    }

    private void loadDribbleData(final ShotsListViewAdapter mAdapter, final ListView shotsListView, final Integer lPage){


        Callback<Popular> callback = new Callback<Popular>() {

            @Override
            public void success(Popular popular, Response response) {


                shots.addAll(popular.getShots());

                if(lPage == 1){

                    // sets the adapter
                    shotsListView.setAdapter(
                            mAdapter
                    );

                    // bind click events on the items
                    shotsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View itemView, int position, long id) {

                            // Calls the shot details in a new activity
                            Intent intent = new Intent(getActivity(), ShotDetailActivity.class);
                            intent.putParcelableArrayListExtra("shots", shots);
                            intent.putExtra("shotPosition", position);
                            startActivity(intent);
                            //getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.pull_out_right);

                        }
                    });

                    // Add the progressBar on the footer
                    View loadMoreView = getActivity().getLayoutInflater().inflate(R.layout.listview_footerview, null);
                    shotsListView.addFooterView(loadMoreView);

                }else{

                    mAdapter.notifyDataSetChanged();

                }

                shotsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView view,
                                                     int scrollState) {
                        // Do nothing
                    }

                    boolean flag_loading = false;

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem,
                                         int visibleItemCount, int totalItemCount) {

                        if (firstVisibleItem + visibleItemCount == totalItemCount
                                && totalItemCount != 0) {
                            if (!flag_loading) {
                                flag_loading = true;

                                page++;
                                loadDribbleData(mAdapter, shotsListView, page);
                            }
                        }

                    }
                });


            }

            @Override
            public void failure(RetrofitError error) {
                // TODO: manage errors
            }
        };

        // Call for the data of Dribbble API
        RestClient.getDribbleMostPopular(lPage, callback);


    }
}
