package eden.concretesolutions.com.br.desafioandroid.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.SyncStateContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;

public class ShotDetailFragment extends Fragment {

    public ShotDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.shot_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final AtomicInteger position = new AtomicInteger();
        final ArrayList<Parcelable> shots;

        Bundle bundle_old = this.getArguments();
        if(bundle_old != null)
        {
            shots = bundle_old.getParcelableArrayList("shots");
            position.set(bundle_old.getInt("shotPosition"));

        }
        else
        {
            Intent intent = getActivity().getIntent();
            shots = intent.getParcelableArrayListExtra("shots");
            position.set(intent.getIntExtra("shotPosition", 0));
        }

        Bundle bundle = new Bundle();
        bundle.putParcelable("shot", shots.get(position.get()));
        getActivity().setTitle( ((Shot) shots.get(position.get())).getTitle() );

        Fragment headerShotDetailFragment = new HeaderShotDetailFragment();
        headerShotDetailFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.header_container, headerShotDetailFragment)
//                .addToBackStack(null)
                .commit();

        Fragment descriptionShotDetailFragment = new DescriptionShotDetailFragment();
        descriptionShotDetailFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.description_container, descriptionShotDetailFragment)
//                .addToBackStack(null)
                .commit();



        final GestureDetector gesture = new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                           float velocityY) {
                        final int SWIPE_MIN_DISTANCE = 120;
                        final int SWIPE_MAX_OFF_PATH = 250;
                        final int SWIPE_THRESHOLD_VELOCITY = 200;

                        try {

                            Bundle bundle = new Bundle();
                            bundle.putParcelableArrayList("shots", shots);

                            Fragment shotDetailFragment = new ShotDetailFragment();
                            shotDetailFragment.setArguments(bundle);

                            FragmentTransaction ft = getFragmentManager().beginTransaction();

                            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                                return false;
                            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                                bundle.putInt("shotPosition", position.addAndGet(1));
                                ft.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);

                            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                                if(position.get() <= 0)
                                    return false;

                                bundle.putInt("shotPosition", position.decrementAndGet());
                                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            }

                            ft.replace(R.id.header_container, shotDetailFragment);
                            ft.addToBackStack(null).commit();


                        } catch (Exception e) {
                            // nothing
                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gesture.onTouchEvent(event);
            }
        });

    }

}