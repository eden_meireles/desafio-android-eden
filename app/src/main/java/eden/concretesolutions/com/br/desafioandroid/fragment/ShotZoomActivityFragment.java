package eden.concretesolutions.com.br.desafioandroid.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;
import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * A placeholder fragment containing a simple view.
 */
public class ShotZoomActivityFragment extends Fragment {

    PhotoViewAttacher mAttacher;

    public ShotZoomActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shot_zoom, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Intent intent = getActivity().getIntent();
        final Shot shot = intent.getParcelableExtra("shot");


        final ImageView mImageView = (ImageView) view.findViewById(R.id.shot_image_zoom);
        Target target = new Target() {

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                mImageView.setImageBitmap(bitmap);

                Drawable image = mImageView.getDrawable();
                mImageView.setImageDrawable(image);
                mAttacher = new PhotoViewAttacher(mImageView);

            }

        };

        Picasso.with(getActivity())
                .load(shot.getImage400Url())
                .into(target);

    }
}
