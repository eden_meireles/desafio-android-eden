package eden.concretesolutions.com.br.desafioandroid.webservice;

import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Popular;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Shot;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface DribbbleApi {

    @GET("/shots/popular")
    void getMostPopular(@Query("page") int page, Callback<Popular> shots);

    @GET("/shots/{shotId}")
    void getShotDetail(@Path("shotId") int shotId, Callback<Shot> shotDetail);

}
