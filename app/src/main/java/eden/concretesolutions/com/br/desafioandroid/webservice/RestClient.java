package eden.concretesolutions.com.br.desafioandroid.webservice;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eden.concretesolutions.com.br.desafioandroid.R;
import eden.concretesolutions.com.br.desafioandroid.ShotDetailActivity;
import eden.concretesolutions.com.br.desafioandroid.webservice.beans.Popular;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class RestClient
{

    public static final String DRIBBBLE_API_ENDPOINT = "http://api.dribbble.com/";

    public static void getDribbleMostPopular(final Integer lPage, Callback<Popular> callback) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(RestClient.DRIBBBLE_API_ENDPOINT)
                .build();

        DribbbleApi service = restAdapter.create(DribbbleApi.class);
        service.getMostPopular(lPage, callback);
    }

}