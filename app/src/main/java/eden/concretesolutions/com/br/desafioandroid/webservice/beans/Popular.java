package eden.concretesolutions.com.br.desafioandroid.webservice.beans;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Popular {

    @Expose
    private String page;
    @SerializedName("per_page")
    @Expose
    private Integer perPage;
    @Expose
    private Integer pages;
    @Expose
    private Integer total;
    @Expose
    private List<Shot> shots = new ArrayList<Shot>();

    /**
     *
     * @return
     * The page
     */
    public String getPage() {
        return page;
    }

    /**
     *
     * @param page
     * The page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     *
     * @return
     * The perPage
     */
    public Integer getPerPage() {
        return perPage;
    }

    /**
     *
     * @param perPage
     * The per_page
     */
    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    /**
     *
     * @return
     * The pages
     */
    public Integer getPages() {
        return pages;
    }

    /**
     *
     * @param pages
     * The pages
     */
    public void setPages(Integer pages) {
        this.pages = pages;
    }

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The shots
     */
    public List<Shot> getShots() {
        return shots;
    }

    /**
     *
     * @param shots
     * The shots
     */
    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }

}